<?php

/**
 * Implements hook_features_export().
 */
function bean_type_features_export($data, &$export, $module_name = '') {
  $pipe = array();

  $map = features_get_default_map('bean_type');
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['features'] = 'bean_features';
  $export['dependencies']['bean'] = 'bean';
  
  if ($bean_types = bean_get_types()) {;
    foreach ($data as $type) {
      $info = $bean_types[$type]->getInfo();
      $export['dependencies'][$info['module']] = $info['module'];
      $export['features']['bean_type'][$type] = $type;

      $fields = field_info_instances('bean', $type);
      foreach ($fields as $name => $field) {
        $pipe['field'][] = "bean-{$field['bundle']}-{$field['field_name']}";
      }
    }
  }

  return $pipe;
}

/**
 * Implements hook_features_export_options().
 */
function bean_type_features_export_options() {
  $feature_types = array();
  $bean_types = bean_get_types();
  if (!empty($bean_types)) {
    foreach($bean_types as $type) {
      $feature_types[$type->type] = $type->getLabel();
    }
  }
  return $feature_types;
}

/**
 * Implements hook_features_export_render().
 */
function bean_type_features_export_render($module, $data, $export = NULL) {
  $info = bean_get_types();
  $output = array();
  $output[] = '  $items = array(';
  
  foreach ($data as $type) {
    if (isset($info[$type]) && $bean_type = $info[$type]) {
      $output[] = "    '{$type}' => array(";
      $info = $bean_type->getInfo();
      foreach($info as $key => $value) {
        $output[] = "      '{$key}' => '{$value}',";
      }
      $output[] = "    ),";
    }
  }
  $output[] = '  );';
  $output[] = '  return $items;';
  $output = implode("\n", $output);
  return array('bean_type_default_types' => $output);
}

/**
 * Implements hook_features_revert().
 */
function bean_type_features_revert($module = NULL) {
  // Get default bean types
  if (module_hook($module, 'bean_type_default_types')) {
    $default_types = module_invoke($module, 'bean_type_default_types');
    $existing_types = bean_get_types();
    
    foreach ($default_types as $type) {
      // Add / or update
      if (!isset($existing_types[$type['type']])) {
        $type['is_new'] = TRUE;
      }
      
      // Use UI function because it provides already the logic we need
      bean_type_features_bean_type_save($type);
    }
  }
  else {
    drupal_set_message(t('Could not load default bean types.'), 'error');
    return FALSE;
  }

  // Re-Cache
  bean_reset();

  menu_rebuild();
  return TRUE;
}

/**
 * Implements hook_features_rebuild().
 */
function bean_type_features_rebuild($module) {
  return bean_type_features_revert($module);
}

/**
 * Saves a bean type.
 *
 * This function will either insert a new bean type if $bean_type['is_new']
 * is set or attempt to update an existing bean type if it is not.
 *
 * @param $bean_type
 *   The bean type array containing the basic properties
 *
 * @return
 *   The return value of the call to drupal_write_record() to save the bean
 *     type, either FALSE on failure or SAVED_NEW or SAVED_UPDATED indicating
 *     the type of query performed to save the product type.
 */
function bean_type_features_bean_type_save($bean_type) {
  $op = drupal_write_record('bean_type', $bean_type, empty($bean_type['is_new']) ? 'type' : array());

  // If this is a new product type and the insert did not fail...
  if (!empty($bean_type['is_new']) && $op !== FALSE) {
    // Notify the field API that a new bundle has been created.
    field_attach_create_bundle('bean_type', $bean_type['type']);
  }

  // Rebuild the menu to get add this type's product add menu item.
  bean_reset();
  menu_rebuild();

  return $op;
}